import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inetum_test/Core/Theme/theme.dart';

class ProgressIndicatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(8.0),
        child: Center(
          child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(lightOrangeColor)),
        ));
  }
}
