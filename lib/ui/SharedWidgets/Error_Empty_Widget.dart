import 'package:flutter/material.dart';
import 'package:inetum_test/Core/Theme/theme.dart';

class ErrorEmptyWidget extends StatelessWidget {
  final Function() onPress;
  final double height;
  final double width;
  final String buttonTitle;
  final String bodyTitle;
  final String body;

  const ErrorEmptyWidget({
    Key key,
    @required this.onPress,
    this.height = double.infinity,
    this.width = double.infinity,
    this.buttonTitle = "Try Again",
    this.bodyTitle = "Oh no!",
    this.body = "Something went wrong \nPlease try again.",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            "assets/Error.png",
            fit: BoxFit.cover,
          ),
          const SizedBox(height: 10),
          // ignore: deprecated_member_use
          FlatButton(
            color: lightOrangeColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            onPressed: () => this.onPress(),
            child: Text(
              buttonTitle.toUpperCase(),
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
          const SizedBox(height: 10),
          Text(
            this.bodyTitle,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 10),
          Text(
            this.body,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w200),
          )
        ],
      ),
    );
  }
}
