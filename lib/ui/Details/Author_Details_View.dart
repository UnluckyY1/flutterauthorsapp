import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'widgets/background_sliver.dart';
import 'widgets/body_sliver.dart';
import 'widgets/button_back.dart';
import 'widgets/cover_photo.dart';
import 'widgets/cut_rectangle.dart';
import 'widgets/data_cut_rectangle.dart';
import 'widgets/mail_circle.dart';
import 'package:inetum_test/Core/model/Authors.dart';

class HomeSliverChallenge extends StatefulWidget {
  final Authors author;
  HomeSliverChallenge({Key key, @required this.author}) : super(key: key);

  @override
  _HomeSliverChallengeState createState() => _HomeSliverChallengeState();
}

class _HomeSliverChallengeState extends State<HomeSliverChallenge> {
  final ScrollController _scrollController = new ScrollController();

  @override
  void dispose() {
    super.dispose();

    /// dispose ScrollController
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          controller: _scrollController,
          physics: BouncingScrollPhysics(),
          slivers: [
            SliverPersistentHeader(
              pinned: true,
              delegate: _SliverAppBar(
                  minExtended: kToolbarHeight * 0.90,
                  maxExtended: size.height * 0.35,
                  size: size,
                  author: widget.author),
            ),
            SliverToBoxAdapter(
              child: Body(
                scrollController: _scrollController,
                size: size,
                author: widget.author,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _SliverAppBar extends SliverPersistentHeaderDelegate {
  const _SliverAppBar({
    @required this.author,
    @required this.maxExtended,
    @required this.minExtended,
    @required this.size,
  });

  /// The size of the header when it is not shrinking
  final double maxExtended;

  /// The smallest size to allow the header to reach
  final double minExtended;
  final Size size;
  final Authors author;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    /// returns the scrolling percent
    final percent = shrinkOffset / maxExtended;

    /// validate the angle at which the card returns
    final uploadlimit = 13 / 100;

    /// return value of the card
    final valueback = (1 - percent - 0.77).clamp(0, uploadlimit);
    final fixrotation = pow(percent, 1.5);

    final card = _CoverCard(
      size: size,
      percent: percent,
      uploadlimit: uploadlimit,
      valueback: valueback,
      author: author,
    );

    final bottomsliverbar = _CustomBottomSliverBar(
      size: size,
      fixrotation: fixrotation,
      percent: percent,
      author: author,
    );

    return Container(
      child: Stack(
        children: [
          /// The background/cover image Widget
          BackgroundSliver(),

          /// if the scrolling percent > 0.13 [uploadlimit] Returns card on top of bottomsliverbar widget
          if (percent > uploadlimit) ...[
            card,
            bottomsliverbar,

            /// else returned under bottomsliverbar widget
          ] else ...[
            bottomsliverbar,
            card,
          ],
          ButtonBack(
            size: size,
            percent: percent,
            onTap: () => Navigator.pop(context),
          ),
          MailCircle(
            size: size,
            percent: percent,
            author: author,
          )
        ],
      ),
    );
  }

  @override
  double get maxExtent => maxExtended;

  @override
  double get minExtent => minExtended;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      false;
}

class _CoverCard extends StatelessWidget {
  const _CoverCard({
    Key key,
    @required this.size,
    @required this.percent,
    @required this.uploadlimit,
    @required this.valueback,
    @required this.author,
  }) : super(key: key);
  final Size size;
  final double percent;
  final double uploadlimit;
  final num valueback;
  final Authors author;
  final double angleForCard = 6.5;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: size.height * 0.20,
      left: size.width / 24,
      child: Transform(
        alignment: Alignment.topRight,
        transform: Matrix4.identity()
          ..rotateZ(percent > uploadlimit
              ? (valueback * angleForCard)
              : percent * angleForCard),
        child: CoverPhoto(
          size: size,
          author: author,
        ),
      ),
    );
  }
}

class _CustomBottomSliverBar extends StatelessWidget {
  const _CustomBottomSliverBar({
    Key key,
    @required this.size,
    @required this.fixrotation,
    @required this.percent,
    @required this.author,
  }) : super(key: key);
  final Size size;
  final num fixrotation;
  final double percent;
  final Authors author;

  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 0,
        left: -size.width * fixrotation.clamp(0, 0.35),
        right: 0,
        child: _CustomBottomSliver(
          size: size,
          percent: percent,
          author: author,
        ));
  }
}

class _CustomBottomSliver extends StatelessWidget {
  final Authors author;
  const _CustomBottomSliver(
      {Key key,
      @required this.size,
      @required this.percent,
      @required this.author})
      : super(key: key);

  final Size size;
  final double percent;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size.height * 0.12,
      child: Stack(
        fit: StackFit.expand,
        children: [
          CustomPaint(
            painter: CutRectangle(),
          ),
          DataCutRectangle(
            size: size,
            percent: percent,
            author: this.author,
          )
        ],
      ),
    );
  }
}
