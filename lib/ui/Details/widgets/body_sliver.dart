import 'package:flutter/material.dart';
import 'package:inetum_test/Core/model/Authors.dart';
import 'package:inetum_test/ui/Details/Posts/Post_List_View.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
    @required this.size,
    @required this.author,
    @required this.scrollController,
  }) : super(key: key);

  final Size size;
  final Authors author;
  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //TODO ADD ACTIVITY Section showing Author comments...
         PostListWidget(
            scrollController: scrollController,
            author: author,
          ),
        
      ],
    );
  }
}
