import 'package:flutter/material.dart';
import 'package:inetum_test/Core/Theme/theme.dart';
import 'package:inetum_test/Core/model/Authors.dart';
import 'package:url_launcher/url_launcher.dart';

class MailCircle extends StatelessWidget {
  const MailCircle({
    Key key,
    @required this.size,
    @required this.percent,
    @required this.author,
  }) : super(key: key);

  final Size size;
  final double percent;
  final Authors author;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: size.height * 0.10,
      right: 10,
      child: percent < 0.2
          ? TweenAnimationBuilder<double>(
              tween: percent < 0.17
                  ? Tween(begin: 1, end: 0)
                  : Tween(begin: 0, end: 1),
              duration: const Duration(milliseconds: 300),
              builder: (context, value, widget) {
                return Transform.scale(
                  scale: 1.0 - value,
                  child: CircleAvatar(
                      minRadius: 20,
                      backgroundColor: lightOrangeColor,
                      child: IconButton(
                        icon: Icon(
                          Icons.mail,
                          color: deepOrangeColor,
                        ),
                        onPressed: () {
                          _launchURL(author.email);
                        },
                      )),
                );
              })
          : const SizedBox(),
    );
  }

  void _launchURL(String mail) async => await canLaunch('mailto:$mail')
      ? await launch('mailto:$mail')
      : throw 'Could not launch $mail';
}
