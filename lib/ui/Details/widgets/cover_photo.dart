import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:inetum_test/Core/model/Authors.dart';


class CoverPhoto extends StatelessWidget {
  const CoverPhoto({
    Key key,
    @required this.size, @required this.author,
  }) : super(key: key);

  final Size size;
  final Authors author;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      width: size.width * 0.25,
      height: size.height * 0.15,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: CachedNetworkImage(
              imageUrl: author?.avatarUrl??"",
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
              fit: BoxFit.fill,
            ),
        
      ),
    );
  }
}
