import 'package:flutter/material.dart';
import 'package:inetum_test/Core/model/Authors.dart';


class CustomBottomDescription extends StatelessWidget {
  final Authors author;
  const CustomBottomDescription({
    Key key, @required this.author,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          author.userName,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
        ),
        SizedBox(
          height: 2,
        ),
        Text(
          author.email,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
        )
      ],
    );
  }
}
