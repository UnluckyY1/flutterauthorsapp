import 'package:flutter/material.dart';
import 'package:inetum_test/Core/ViewModel/Posts_model.dart';
import 'package:inetum_test/Core/enums/viewstate.dart';
import 'package:inetum_test/Core/model/Authors.dart';
import 'package:inetum_test/ui/BaseView/base_view.dart';
import 'package:inetum_test/ui/Details/Posts/Widgets/Authorpost.dart';
import 'package:inetum_test/ui/SharedWidgets/Error_Empty_Widget.dart';
import 'package:inetum_test/ui/SharedWidgets/ProgressIndicatorWidget.dart';

class PostListWidget extends StatefulWidget {
  const PostListWidget(
      {Key key, @required this.author, @required this.scrollController})
      : super(key: key);

  final Authors author;
  final ScrollController scrollController;

  @override
  _PostListWidgetState createState() => _PostListWidgetState();
}

class _PostListWidgetState extends State<PostListWidget> {
  ScrollController _scrollController;
  PostListModel _postsModel;
  int authorID;

  @override
  void initState() {
    super.initState();
    authorID = widget.author.id;
    _scrollController = widget.scrollController;
    // add listener if the user reachs the end of the list automatically requests the next page data and remove the listnner if we reach last page [page==-1]
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (_postsModel.page != -1)
          _postsModel.getAuthorPostsList(authorid: widget.author.id);
        else
          _scrollController.removeListener(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<PostListModel>(
      /// requests data from [PostListModel] model
      onModelReady: (model) => model.getAuthorPostsList(authorid: authorID),
      builder: (context, modal, widget) {
        _postsModel = modal;
        if (modal.state == ViewState.Error)
          return ErrorEmptyWidget(
            onPress: () => modal.resetList(authorid: authorID),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          );
        else if (modal.state == ViewState.Busy)
          return ProgressIndicatorWidget();
        else if (modal.posts.isNotEmpty) {
          return ListView.builder(
            itemCount: modal.posts.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return _getItemWidget(index);
            },
          );
        } else
          return ErrorEmptyWidget(
            onPress: () => modal.resetList(authorid: authorID),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            buttonTitle: "Check for new posts",
            body: "This Author dosent have any posts",
          );
      },
    );
  }

  Widget _getItemWidget(int index) {
    if (index == _postsModel.posts.length - 1 && _postsModel.page != -1)
      return ProgressIndicatorWidget();
    return AuthorPost(
      author: widget.author,
      post: _postsModel.posts[index],
    );
  }
}
