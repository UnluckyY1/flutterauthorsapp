import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inetum_test/Core/model/Authors.dart';
import 'package:inetum_test/Core/model/Post.dart';
import 'package:inetum_test/ui/Details/Posts/Widgets/custom_card.dart';
import 'package:inetum_test/ui/Details/Posts/Widgets/custom_image.dart';
import 'package:inetum_test/ui/Details/Posts/Widgets/post_header.dart';
import 'package:readmore/readmore.dart';

class AuthorPost extends StatelessWidget {
  final Post post;
  final Authors author;

  AuthorPost({@required this.post, @required this.author});
  final DateTime timestamp = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(10),
        child: CustomCard(
            borderRadius: BorderRadius.circular(10.0),
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0),
                      ),
                      child: PostImage(
                        imageUrl: post?.imageUrl ?? '',
                        height: 300.0,
                        fit: BoxFit.cover,
                        width: double.infinity,
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Text(
                        post?.title ?? "",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff4D4D4D),
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: ReadMoreText(
                          post?.body ?? '',
                          trimLines: 2,
                          colorClickableText: Color(0xff4D4D4D),
                          trimMode: TrimMode.Line,
                          trimCollapsedText: 'Show more',
                          trimExpandedText: '',
                          delimiterStyle:
                              TextStyle(fontSize: 14, color: Color(0xff4D4D4D)),
                          moreStyle:
                              TextStyle(fontSize: 14, color: Color(0xff4D4D4D)),
                          style:
                              TextStyle(fontSize: 14, color: Color(0xff4D4D4D)),
                        )),
                  ],
                ),
                PostHeader(
                  author: author,
                  post: post,
                )
              ],
            )));
  }
}
