import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:inetum_test/Core/model/Authors.dart';
import 'package:inetum_test/Core/model/Post.dart';
import 'package:timeago/timeago.dart' as timeago;

class PostHeader extends StatelessWidget {
  final Authors author;
  final Post post;

  const PostHeader({@required this.author, @required this.post});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        height: 40.0,
        decoration: BoxDecoration(
          color: Colors.white60,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              author?.avatarUrl != null
                  ? CircleAvatar(
                      radius: 14.0,
                      backgroundColor: Color(0xff4D4D4D),
                      backgroundImage:
                          CachedNetworkImageProvider(author?.avatarUrl ?? ""),
                    )
                  : CircleAvatar(
                      radius: 14.0,
                      backgroundColor: Color(0xff4D4D4D),
                    ),
              SizedBox(width: 5.0),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    author?.name ?? "",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xff4D4D4D),
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Text(timeago.format(DateTime.parse(post.date)),
                        style: TextStyle(fontSize: 10.0)),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
