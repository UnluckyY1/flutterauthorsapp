import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inetum_test/ui/Details/Posts/Widgets/Image_view.dart';



class PostImage extends StatelessWidget {
  final String imageUrl;
  final double height;
  final double width;
  final BoxFit fit;

  PostImage({
    this.imageUrl,
    this.height = 100.0,
    this.width = double.infinity,
    this.fit = BoxFit.cover,
  });

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (BuildContext context, VoidCallback _) {
        return HeroPhotoViewRouteWrapper(
          heroTag: imageUrl ?? "",
          imageProvider: CachedNetworkImageProvider(
            imageUrl ?? "",
          ),
        );
      },
      closedElevation: 0.0,
      closedShape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      closedBuilder: (BuildContext context, VoidCallback openContainer) {
        return CachedNetworkImage(
          imageUrl: imageUrl ?? '',
          errorWidget: (context, url, error) {
            return Icon(Icons.error);
          },
          height: height,
          fit: fit,
          width: width,
        );
      },
    );
  }
}
