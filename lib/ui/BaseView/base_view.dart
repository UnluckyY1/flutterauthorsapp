import 'package:flutter/material.dart';
import 'package:inetum_test/Core/ViewModel/base_model.dart';
import 'package:inetum_test/locator.dart';
import 'package:provider/provider.dart';

/// Most of the views require their own model, they need to have a root widget Provider and a child Consumer that takes a build method
/// We'll create a BaseView that is generic that will do all this for us and use it when we need it.

class BaseView<T extends BaseModel> extends StatefulWidget {
  final Widget Function(BuildContext context, T model, Widget child) builder;
  final Function(T) onModelReady;

  BaseView({this.builder, this.onModelReady});

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends BaseModel> extends State<BaseView<T>> {
  /// requests the generic model from get_it locator
  T model = locator<T>();

  /// requests data from the model
  void initState() {
    if (widget.onModelReady != null) {
      widget.onModelReady(model);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
        create: (context) => model,
        child: Consumer<T>(builder: widget.builder));
  }
}
