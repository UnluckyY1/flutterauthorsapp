import 'package:flutter/material.dart';
import 'package:inetum_test/Core/ViewModel/home_model.dart';
import 'package:inetum_test/Core/enums/viewstate.dart';
import 'package:inetum_test/ui/BaseView/base_view.dart';
import 'package:inetum_test/ui/Home/Widgets/AuthorTile.dart';
import 'package:inetum_test/ui/SharedWidgets/Error_Empty_Widget.dart';
import 'package:inetum_test/ui/SharedWidgets/ProgressIndicatorWidget.dart';

class AuthorListWidget extends StatefulWidget {
  const AuthorListWidget({Key key}) : super(key: key);
  @override
  _AuthorListWidgetState createState() => _AuthorListWidgetState();
}

class _AuthorListWidgetState extends State<AuthorListWidget> {
  final ScrollController _scrollController = new ScrollController();
  AuthorsListModel _authorsModel;

  @override
  void initState() {
    super.initState();
    // add listener if the user reachs the end of the list automatically requests the next page data and remove the listnner if we reach last page [Page==-1]
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (_authorsModel.page != -1)
          _authorsModel.getAuthorsList();
        else
          _scrollController.removeListener(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: BaseView<AuthorsListModel>(
        /// requests data from [PostListModel] model
        onModelReady: (model) => model.getAuthorsList(),

        /// According to the state we gonna return a specific widget
        builder: (context, modal, widget) {
          _authorsModel = modal;
          if (modal.state == ViewState.Error)
            return ErrorEmptyWidget(
              onPress: () => modal.resetList(),
            );
          else if (modal.state == ViewState.Busy)
            return ProgressIndicatorWidget();
          else if (modal.authors.isNotEmpty) {
            return RefreshIndicator(
              child: ListView.builder(
                  controller: _scrollController,
                  itemCount: modal.authors.length,
                  shrinkWrap: false,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return _getItemWidget(index);
                  }),
              onRefresh: () async => await modal.resetList(),
            );
          } else
            return ErrorEmptyWidget(
              onPress: () => modal.resetList(),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              buttonTitle: "Check for new Authors",
              body: "No Author has been found",
            );
        },
      ),
    );
  }

  Widget _getItemWidget(int index) {
    if (index == _authorsModel.authors.length - 1 && _authorsModel.page != -1)
      return ProgressIndicatorWidget();
    return AuthorTile(
      author: _authorsModel.authors[index],
    );
  }
}
