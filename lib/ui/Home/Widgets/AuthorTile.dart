import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:inetum_test/Core/Theme/theme.dart';
import 'package:inetum_test/Core/model/Authors.dart';
import 'package:inetum_test/ui/Details/Author_Details_View.dart';
import 'package:inetum_test/ui/Home/Widgets/AuthorAvatarWidget.dart';
import 'package:url_launcher/url_launcher.dart';

class AuthorTile extends StatelessWidget {
  final Authors author;

  const AuthorTile({Key key, this.author}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      transitionType: ContainerTransitionType.fadeThrough,
      openBuilder: (BuildContext context, VoidCallback _) {
        return HomeSliverChallenge(
          author: this.author,
        );
      },
      closedElevation: 0.0,
      closedShape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      closedBuilder: (BuildContext context, void Function() action) {
        return Container(
          decoration: BoxDecoration(
              color: Color(0xffFFEEE0),
              borderRadius: BorderRadius.circular(20)),
          margin: const EdgeInsets.only(bottom: 10),
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
          child: Row(
            children: <Widget>[
              AuhtorAvatarWidget(
                avatarUrl: author.avatarUrl,
              ),
              SizedBox(
                width: 17,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      author.name ?? "",
                      maxLines: 1,
                      style: TextStyle(color: deepOrangeColor, fontSize: 19),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      author.userName,
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () => _launchURL(author.email),
                child: Container(
                  margin: EdgeInsets.only(left: 5),
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 9),
                  decoration: BoxDecoration(
                      color: lightOrangeColor,
                      borderRadius: BorderRadius.circular(13)),
                  child: Text(
                    "E-Mail",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void _launchURL(String mail) async => await canLaunch('mailto:$mail')
      ? await launch('mailto:$mail')
      : throw 'Could not launch $mail';
}
