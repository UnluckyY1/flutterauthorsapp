import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:inetum_test/Core/Theme/theme.dart';

class AuhtorAvatarWidget extends StatelessWidget {
  const AuhtorAvatarWidget({Key key, @required this.avatarUrl})
      : super(key: key);
  final String avatarUrl;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: CachedNetworkImage(
        imageUrl: avatarUrl,
        placeholder: (context, url) => Container(
          height: 50.0,
          width: 60,
          color: lightOrangeColor,
        ),
        errorWidget: (context, url, error) {
          return Container(
              height: 50.0,
              width: 60,
              color: lightOrangeColor,
              child: Icon(Icons.error));
        },
        height: 60,
        fit: BoxFit.cover,
      ),
    );
  }
}
