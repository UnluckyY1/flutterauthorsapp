import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import 'network_exceptions.dart';

class ApiProvider {
  // API base config
  static final String baseUrl = "https://sym-json-server.herokuapp.com/";

  static final BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 10000,
      receiveTimeout: 10000,
      method: 'GET',
      contentType: Headers.jsonContentType);

  static final Dio dio = new Dio(options);

  /// get authorsList
  Future<List<dynamic>> getAuthorsList({@required int page}) async {
    /// debug the output from api for testing
    //dio.interceptors.add(PrettyDioLogger());
    try {
      /// requests data
      final res = await dio.get("/authors", queryParameters: {
        "_limit": 10,
        "_page": page,
      });
      print(res.headers);

      /// return Response body
      return res.data as List;
    } catch (e) {
      ///throws dio/Network Exception as String
      return throw NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }
  }

  /// get author Posts List
  Future<List<dynamic>> getAuthorPosts(
      {@required int page, @required int authorid}) async {
    try {
      /// requests data
      final res = await dio.get("/posts", queryParameters: {
        "authorId": authorid,
        "_limit": 10,
        "_page": page,
      });

      /// return Response body
      return res.data as List;
    } catch (e) {
      ///throws dio/Network Exception as String
      return throw NetworkExceptions.getErrorMessage(
          NetworkExceptions.getDioException(e));
    }
  }
}
