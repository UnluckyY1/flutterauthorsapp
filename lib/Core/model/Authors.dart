class Authors {
  int id;
  String name;
  String userName;
  String email;
  String avatarUrl;
  Address address;

  Authors(
      {this.id,
      this.name,
      this.userName,
      this.email,
      this.avatarUrl,
      this.address});

  Authors.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    userName = json['userName'];
    email = json['email'];
    avatarUrl = json['avatarUrl'];
    address = new Address.fromJson(json['address']);
  }
}

class Address {
  String latitude;
  String longitude;

  Address({this.latitude, this.longitude});

  Address.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}
