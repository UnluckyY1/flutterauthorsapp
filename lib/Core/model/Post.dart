class Post {
  int id;
  String date;
  String title;
  String body;
  String imageUrl;
  int authorId;

  Post(
      {this.id,
      this.date,
      this.title,
      this.body,
      this.imageUrl,
      this.authorId});

  Post.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
    title = json['title'];
    body = json['body'];
    imageUrl = json['imageUrl'];
    authorId = json['authorId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date'] = this.date;
    data['title'] = this.title;
    data['body'] = this.body;
    data['imageUrl'] = this.imageUrl;
    data['authorId'] = this.authorId;
    return data;
  }
}