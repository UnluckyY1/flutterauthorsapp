import 'package:flutter/material.dart';
import 'package:inetum_test/Core/ViewModel/base_model.dart';
import 'package:inetum_test/Core/enums/viewstate.dart';
import 'package:inetum_test/Core/model/Post.dart';
import 'package:inetum_test/Core/services/ApiProvider.dart';
import 'package:inetum_test/locator.dart';

class PostListModel extends BaseModel {
  /// GET instance of API Class
  final ApiProvider _api = locator<ApiProvider>();

  /// List of Posts
  final List<Post> posts = [];
  int page = 1;
  String errorMsg = "";

  Future<void> resetList({@required int authorid}) async {
    /// resetting page to the first page
    page = 1;

    /// setting State to Busy
    setState(ViewState.Busy);

    /// Clearing the error msg if there is one
    errorMsg = "";

    /// Clearing the list
    posts.clear();

    /// notifying listeners
    notify();

    ///
    await getAuthorPostsList(authorid: authorid);
  }

  Future<void> getAuthorPostsList({@required int authorid}) async {
    if (page != -1) {
      try {
        /// requestion data from api
        final data = await _api.getAuthorPosts(page: page, authorid: authorid);

        ///serialization of the data
        final List<Post> items =
            data.map<Post>((item) => Post.fromJson(item)).toList();

        /// Checks if the requested page has data which means that can be data on the next page if not that means we reached the last page
        page = items.isNotEmpty ? page + 1 : -1;
        print("is last page ${page==-1}");

        /// adding the new posts to the list
        if (items != null && items.isNotEmpty) posts.addAll(items);

        /// setting state to idle
        setState(ViewState.Idle);

        /// notifying listeners
        notify();
        return;
      } catch (e) {
        print(
            "************************************Error************************************");
        print(e);
        errorMsg = e.toString();

        /// setting state to Error
        setState(ViewState.Error);

        /// notifying listeners
        notify();
      }
    }
  }
}
