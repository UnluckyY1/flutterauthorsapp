import 'dart:core';
import 'package:inetum_test/Core/ViewModel/base_model.dart';
import 'package:inetum_test/Core/enums/viewstate.dart';
import 'package:inetum_test/Core/model/Authors.dart';
import 'package:inetum_test/Core/services/ApiProvider.dart';
import 'package:inetum_test/locator.dart';

class AuthorsListModel extends BaseModel {
  /// GET instance of API Class with locator
  final ApiProvider _api = locator<ApiProvider>();

  /// initialise our  List of Posts as empty list
  final List<Authors> authors = [];

  /// initialise Page with 1 the first page
  int page = 1;

  /// initialise with an empty error msg since our intial state is busy
  String errorMsg = "";

  /// refrech/reset List
  Future<void> resetList() async {
    /// resetting page to the first page
    page = 1;

    /// setting State to Busy
    setState(ViewState.Busy);

    /// Clearing the error msg if there is one
    errorMsg = "";

    /// Clearing the list
    authors.clear();

    /// notifying listeners
    notify();

    /// make a new request
    await getAuthorsList();
  }

  /// try to get author list
  Future<void> getAuthorsList() async {
    if (page != -1) {
      try {
        /// requestion data from api
        final data = await _api.getAuthorsList(page: page);

        /// serialization of the data
        final List<Authors> items =
            data.map<Authors>((item) => Authors.fromJson(item)).toList();

        /// Checks if the requested page has data which means that can be data on the next page if not that means we reached the last page
        /// this is just a workaround because honestly at first i didn't know how to work with "first","prev"... link in the header response
        page = items.isNotEmpty ? page + 1 : -1;

        /// adding the new posts to the list
        if (items != null && items.isNotEmpty) authors.addAll(items);

        /// setting state to idle
        setState(ViewState.Idle);

        /// notifying listeners
        notify();
        return;
      } catch (e) {
        print(
            "************************************Error************************************");
        print(e);
        errorMsg = e.toString();

        /// setting state to Error
        setState(ViewState.Error);

        /// notifying listeners
        notify();
      }
    }
  }
}
