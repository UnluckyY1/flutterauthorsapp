import 'package:flutter/widgets.dart';
import 'package:inetum_test/Core/enums/viewstate.dart';

/// All our Models will work the same. We have a state property that tells us what UI layout to show
/// in the view and when it's updated we want to call notify()

class BaseModel extends ChangeNotifier {
  /// Setting the state to busy as default
  ViewState _state = ViewState.Busy;

  /// Gets the current State
  ViewState get state => _state;

  ///changes the current State
  void setState(ViewState viewState) {
    _state = viewState;
    notify();
  }

  /// notifyListeners
  void notify() {
    try {
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}
