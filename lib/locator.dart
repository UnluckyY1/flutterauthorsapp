import 'package:get_it/get_it.dart';
import 'package:inetum_test/Core/ViewModel/Posts_model.dart';
import 'package:inetum_test/Core/ViewModel/home_model.dart';
import 'package:inetum_test/Core/services/ApiProvider.dart';

///access getIt instance 
GetIt locator = GetIt.instance;

///Register services with the locator
void setupLocator() {
  locator.registerLazySingleton(() => ApiProvider());
  locator.registerFactory(() => AuthorsListModel());
  locator.registerFactory(() => PostListModel());
  // locator.registerFactory(() => CommentsModel());
}
