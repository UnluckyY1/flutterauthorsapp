import 'package:flutter/material.dart';
import 'package:inetum_test/ui/Home/home.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Authors',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
