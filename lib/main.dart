import 'package:flutter/material.dart';
import 'package:inetum_test/App.dart';
import 'package:inetum_test/locator.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}
