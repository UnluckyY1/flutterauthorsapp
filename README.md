# Authors Posts App

This application uses the `provider` architecture (using it only for it's state management) and using `get_it` for dependency injection. Each view has it's own model, any widget with logic would have the same, we have a base view that provides a state enum to listen too and we use services to define all our business logic. 

## Screenshots
<p>
<img src="media/S1.png" width="200"/> <img src="media/S4.png" width="200"/> <img src="media/S2.png" width="200"/> <img src="media/S5.png" width="200"/>  <img src="media/S3.png" width="200"/> 
</p>

## How to use


Step 1:

Download or clone this repository with the following command

```
git clone https://gitlab.com/UnluckyY1/flutterauthorsapp.git
```


Step 2:

At the root of the project run the following command in the console to get the necessary dependencies:

```
flutter pub get
```

Step 3:

At the root of the project run the following command in the console to run the app:

```
flutter run --release
```


## Used Libraries & Packages


- `dio: ^4.0.0`: Allows to make http requests
- `provider: ^6.0.0`: state mangment
- `get_it: ^7.2.0`: To perform dependency injection
- `timeago: ^3.1.0`: To fromat the date of the post
- `cached_network_image: ^3.1.0`: For caching and showing pictures from the internet
- `animations: ^2.0.1`: For adding some beautiful animations to the app
- `readmore`: For expand and collapse authors posts text
- `photo_view: ^0.10.3`: enables images to become able to zoom and pan with user gestures such as pinch, rotate and drag
- `url_launcher: ^6.0.10`: allow user to send emails to authors

## Characteristics

- The application has been structured following Clean Architecture
- Consumption of an Api Rest
- Display information in a beautiful animated interface




## High Level Architecture Overview
- Each view will have it's own model that extends the ChangeNotifier.
- Notify listeners for a view will ONLY BE CALLED when the View's state changes.
- Each view only has 3 states. Idle, Busy and Error . Any other piece of UI contained in a view, that requires logic and state / UI updates will have it's own model associated with it. This way the main view only paints when the main view state changes.
- View Models will ONLY request data from Services and reduce state from that DATA. Nothing else.

## Project structure

The lib folder is divided into two folders. core and ui. Core contains all the files associated with the logic. ui contains all the files associated with the ui. 
Core is divided into 4 folders.

- enums: Contains ViewState enum
- Models: Contains all the plain data models
- Services: Contains the dedicated files that will handle actual business logic
- ViewModels: Contains the Provider models for each of the Widget views.

UI is also divided into 4 folders 

Note: Every folder below except BaseView Contains a sub-folder called Widgets contains widget files that are too big to keep in the main view files.
- BaseView: Contains the base_view.dart file that any widget that requires a viewmodel
- Home: Contains the files the home screen
- SharedWidgets: Contains files used in multiple other UI files
- Author details : Contains the files the Author details screen






